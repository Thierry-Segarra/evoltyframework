<?php

namespace EvoltyFramework\Database;

use \PDO;
use \PDOException;
use EvoltyFramework\Tools\Env;
use Exception;

class DatabaseManager {
    private static $instance;
    private $db;

    private function __construct() {
        try {
            // Initialize the Env class to load environment variables
            new Env();

            // Retrieve the REWRITE_BASE value from environment variables
            $host = $_ENV['BDD_HOST'] ?? null;
            $port = $_ENV['BDD_PORT'] ?? null;
            $username = $_ENV['BDD_USERNAME'] ?? null;
            $password = $_ENV['BDD_PASSWORD'] ?? null;
            $database = $_ENV['BDD_DATABASE'] ?? null;
            $type = $_ENV['BDD_TYPE'] ?? null;

            // Check if REWRITE_BASE is set
            if (!$type ||!$host || !$username || !$password || !$database) {
                throw new Exception("BDD environment variable is not set.");
            }

            if($type == 'POSTGRESQL'){
                $dsn = "pgsql:host=$host;port=$port;dbname=$database";
                
            }else if($type == 'MYSQL'){
                $dsn = "mysql:host=$host;dbname=$database";

            }else {
                throw new Exception("BDD type environment not found.");
            }
            $this->db = new PDO($dsn, $username, $password);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die("Database connection failed: " . $e->getMessage());
        }
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getDb() {
        return $this->db;
    }
}
