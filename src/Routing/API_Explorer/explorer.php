<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        <?php require_once("assets/css/style.css") ?>
    </style>
    <title>API Documentation</title>
</head>
<body>
    <div>
        <h2>Login - Logout</h2>
        <div id="Login" style="display: block;">
            <input id="inputLogin" type="text" placeholder="Token Login">
            <button onclick="login()">Login</button>
        </div>
        <div id="Logout" style="display: none;">
            <button onclick="logout()">Logout</button>
        </div>
    </div>
    <?php 
        $id = 1;
        foreach ($routes as $group) :
    ?>
        <div class="route-container">
            <h2><?= htmlspecialchars($group->groupe) ?></h2>
            <p><?= htmlspecialchars($group->description) ?></p>
            <button onclick='Views(this,"<?= "groupe_" . $id ?>")'>Show More</button>
            <div class="route-container" id="<?= "groupe_" . $id ?>" style="display: none;">
                <?php foreach ($group->routes as $route) : ?>
                    <div class="route-container">
                        <div class="route-method"><?= htmlspecialchars($route->method) ?></div>
                        <div class="route-path"><?= htmlspecialchars($route->route) ?></div>
                        <div class="route-description"><?= htmlspecialchars($route->description) ?></div>
                        <div class="route-security"><?= securityRoute($route->security) ?></div>
                        <div>
                            <?php if ($route->method != "GET") { ?>
                            <button onclick='ChangeInterface("<?= "Json_form_" . $id ?>","<?= "front_form_" . $id ?>")'>Json/Front</button>
                            <div class="parameters" id="<?= "Json_form_" . $id ?>" style="display: block;">
                                <strong>Json:</strong>
                                <form class="jsonEnvoi">
                                    <textarea id="<?= "Route_Json_" . $id ?>" cols="30" rows="10"><?= htmlspecialchars(JsonGeneration($route->parameters)) ?></textarea>
                                    <button type="button" onclick="FetchDataJson('<?= $id ?>', '<?= $route->method ?>', '<?= htmlspecialchars($url . $route->route) ?>' , <?= verifSecurity($route->security) ?>)">Executer</button>
                                </form>
                            </div>
                            <div class="parameters" id="<?= "front_form_" . $id ?>" style="display: none;">
                                <?php } else { ?>
                                    <div class="parameters" id="<?= "front_form_" . $id ?>" style="display: block;">
                                <?php } ?>
                                    <strong>Parameters:</strong>
                                    <form id="<?= "Route" . $id ?>">
                                        <?php foreach ($route->parameters as $param) : ?>
                                            <?= htmlspecialchars($param['name'] . ' : ' . $param['description']) ?><br>
                                            <?= inputRoute($param['name'], $param['type'], $param['exemple']); ?>
                                        <?php endforeach; ?>
                                        <button type="button" onclick="FetchData('<?= $id ?>', '<?= $route->method ?>', '<?= htmlspecialchars($url . $route->route) ?>', <?= verifSecurity($route->security) ?>)">Executer</button>
                                    </form>
                                </div>
                                <div class="parameters">
                                    <div>
                                        <strong>Status: </strong><strong id="<?= "CodeStatus" . $id ?>"></strong>
                                    </div>
                                    <strong>Result:</strong>
                                    <div class="jsonEnvoi">
                                        <textarea id="<?= "Route" . $id . "result" ?>" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                    $id++;
                endforeach;
                ?>
                </div>
            </div>
            <?php
        endforeach;
        ?>
        </div>

    <?php
    function securityRoute($security) {
        return $security ? "Security" : "";
    }
    function verifSecurity($security) {
        return $security ? true : false;
    }

    function inputRoute($id_name, $type, $exemple) {
        $type_input = 'text';
        switch (strtolower($type)) {
            case 'int':
            case 'integer':
            case 'number':
                $type_input = 'number';
                break;
            case 'date':
                $type_input = 'date';
                break;
            case 'string':
            default:
                $type_input = 'text';
                break;
        }
        return "<input id='" . htmlspecialchars($id_name) . "' name='" . htmlspecialchars($id_name) . "' type='" . htmlspecialchars($type_input) . "' value='" . htmlspecialchars($exemple) . "'><br>";
    }

    function JsonGeneration($params) {
        $json = '{';
        $lastParam = end($params);
        foreach ($params as $param) {
            $json .= '"' . htmlspecialchars($param['name']) . '":' . TypeValue($param['type'], $param['exemple']) . ($param !== $lastParam ? ',' : '');
        }
        $json .= '}';
        return $json;
    }

    function TypeValue($type, $exemple = "") {
        switch (strtoupper($type)) {
            case 'INT':
            case 'INTEGER':
            case 'NUMBER':
                return $exemple;
            case 'STRING':
            case 'DATE':
            default:
                return '"' . htmlspecialchars($exemple) . '"';
        }
    }
    ?>

    <script>
        function ChangeInterface(nom_div1, nom_div2) {
            const div1 = document.getElementById(nom_div1);
            const div2 = document.getElementById(nom_div2);
            if (div1.style.display === "block") {
                div1.style.display = "none";
                div2.style.display = "block";
            } else {
                div1.style.display = "block";
                div2.style.display = "none";
            }
        }

        function FetchData(id, method, route, security) {
            const formElement = document.getElementById('Route' + id);
            if (!formElement) return;

            const formData = new FormData(formElement);

            const fetchOptions = {
                headers: {
                    'Accept': 'application/json'
                },
                method: method
            };

            if (security) {
                const token = getCookie('token');
                if (token) {
                    fetchOptions.headers['Authorization'] = 'Bearer ' + token;
                } else {
                    console.error('Token not found in cookies');
                    // return;
                }
            }

            if (method.toUpperCase() === 'PUT' || method.toUpperCase() === 'DELETE') {
                const formDataObject = {};
                formData.forEach((value, key) => formDataObject[key] = value);
                fetchOptions.body = JSON.stringify(formDataObject);
            } else if (method.toUpperCase() === 'GET' || method.toUpperCase() === 'HEAD') {
                route += '?' + new URLSearchParams(formData).toString();
            } else {
                fetchOptions.body = formData;
            }
            console.log(route);
            fetch(route, fetchOptions)
                .then(response => {
                    statusEnvoi(id,response.status)
                    return response.json();
                })
                .then(data => {
                    resultatEnvoi(id,data)
                })
                .catch(error => {
                    console.error('Erreur lors de l\'envoi des données :', error);
                });
        }

        function FetchDataJson(id, method, route, security) {
            console.log(route);
            const jsonData = document.getElementById('Route_Json_' + id).value;
            if (!isValidJson(jsonData)) {
                statusEnvoi(id,'400');
                var erreur = JSON.stringify({ 'erreur': "The JSON is invalid." });
                resultatEnvoi(id,erreur,true)
                return;
            }

            const fetchOptions = {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: method,
                body: jsonData
            };

            if (security) {
                const token = getCookie('token');
                if (token) {
                    fetchOptions.headers['Authorization'] = 'Bearer ' + token;
                } else {
                    console.error('Token not found in cookies');
                    // return;
                }
            }

            fetch(route, fetchOptions)
            .then(response => {
                statusEnvoi(id,response.status)
                return response.json();
            })
            .then(data => {
                console.log(data);
                resultatEnvoi(id,data)
            })
            .catch(error => {
                console.error('Erreur lors de l\'envoi des données :', error);
            });
        }

        function Views(balise,idGroupe) {
            var displayGroupe =document.getElementById(idGroupe).style.display;
            if (displayGroupe == 'none') {
                document.getElementById(idGroupe).style.display = 'block';
                balise.innerHTML = "Show Less"
            }else{
                document.getElementById(idGroupe).style.display = 'none';
                balise.innerHTML = "Show More"
            }
        }

        function setCookie(name, value, days) {
            let expires = "";
            if (days) {
                let date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + (value || "") + expires + "; path=/;secure=true";
        }

        function getCookie(name) {
            let nameEQ = name + "=";
            let ca = document.cookie.split(';');
            for(let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

        function eraseCookie(name) {
            document.cookie = name + "=; Expires=Thu, 01 Jan 1970 00:00:01 GMT; Path=/;secure=true";
        }

        document.addEventListener("DOMContentLoaded", function() {
            // Check if token exists in cookies on page load
            if (getCookie('token')) {
                document.getElementById('Login').style.display = 'none';
                document.getElementById('Logout').style.display = 'block';
            } else {
                document.getElementById('Login').style.display = 'block';
                document.getElementById('Logout').style.display = 'none';
            }
        });

        function login() {
            const token = document.getElementById('inputLogin').value;
            
            if (token) {
                setCookie('token', token, 7); // Token expires in 7 days
                alert('Token saved in cookies');
                document.getElementById('Login').style.display = 'none';
                document.getElementById('Logout').style.display = 'block';
            } else {
                alert('Please enter a token.');
            }
        }

        function logout() {
            eraseCookie('token');
            alert('Logged out');
            document.getElementById('Login').style.display = 'block';
            document.getElementById('Logout').style.display = 'none';
        }

        function isValidJson(jsonString) {
            try {
                JSON.parse(jsonString);
                return true; // JSON is valid
            } catch (e) {
                return false; // JSON is invalid
            }
        }

        function statusEnvoi(id,status){
            document.getElementById('CodeStatus'+id).innerHTML = status;
        }
        function resultatEnvoi(id,result,erreur = false){
            if (!erreur) {
                document.getElementById('Route' + id + 'result').value = JSON.stringify(result, null, 2);
            }else{
                document.getElementById('Route' + id + 'result').value = result;
            }
        }
    </script>
</body>
</html>
