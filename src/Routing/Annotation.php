<?php

namespace EvoltyFramework\Routing;

use \stdClass;

class Annotation
{

    function parseAPIRouteAnnotation($annotation)
    {
        $routeInfo = new stdClass(); // Création d'un objet pour stocker les informations
        $routeInfo->parameters = []; // Ajout de la propriété $parameters par défaut

        // Vérification de la présence de l'annotation
        if (!preg_match('/@ApiRoute\s+/', $annotation)) {
            return null; // Retourne null si l'annotation n'est pas valide
        }

        // Extraction du titre
        if (preg_match('/- Groupe: ([^\r\n]+)/', $annotation, $titleMatch)) {
            $routeInfo->groupe = trim($titleMatch[1]);
        }else{
            $routeInfo->groupe = 'Not Defined';
        }

        // Extraction de la méthode
        if (preg_match('/- Method: (\w+)/', $annotation, $methodMatch)) {
            $routeInfo->method = strtoupper($methodMatch[1]); // Assure que la méthode est en majuscules
        } else {
            return null; // Retourne null si la méthode n'est pas trouvée
        }

        // Extraction du chemin
        if (preg_match('/- Path: ([^\s]+)/', $annotation, $routeMatch)) {
            $routeInfo->route = trim($routeMatch[1]); // Assure que le chemin est sans espaces indésirables
        } else {
            return null; // Retourne null si le chemin n'est pas trouvé
        }

        // Extraction de la description
        if (preg_match('/- Description: ([^\r\n]+)/', $annotation, $descriptionMatch)) {
            $routeInfo->description = trim($descriptionMatch[1]);
        }else {
            $routeInfo->description = '';
        }

        // Extraction des paramètres
        if (preg_match('/- Parameters: {([^}]+)}/', $annotation, $parameterMatch)) {
            $parameterInfo = [];
            $parameterLines = explode("\n", $parameterMatch[1]);

            foreach ($parameterLines as $line) {
                if (preg_match('/- (\w+): \((\w+)\) (?:ex:\((\w+)\))?(.+)?/', $line, $parameterMatch)) {
                    $paramName = $parameterMatch[1];
                    $parameterInfo[$paramName] = [
                        'name' => $paramName,  // Ajout du nom du paramètre
                        'type' => $parameterMatch[2],
                        'exemple' => isset($parameterMatch[3]) ? trim($parameterMatch[3]) : null, // exemple est optionnelle
                        'description' => isset($parameterMatch[4]) ? trim($parameterMatch[4]) : '', // Description est optionnelle
                    ];
                }
            }

            $routeInfo->parameters = $parameterInfo;
        }

        if (preg_match('/- Security/', $annotation)) {
            $routeInfo->security = true;
        } else {
            $routeInfo->security = false;
        }

        return $routeInfo;
    }

    public function parseGeneralAnnotation($annotation)
    {
        $groupeInfo = new stdClass();

        // Extraction du titre
        if (preg_match('/- Groupe: ([^\r\n]+)/', $annotation, $titleMatch)) {
            $groupeInfo->groupe = trim($titleMatch[1]);
        }

        // Extraction de la description
        if (preg_match('/- Description: ([^\r\n]+)/', $annotation, $descriptionMatch)) {
            $groupeInfo->description = trim($descriptionMatch[1]);
        }else {
            $groupeInfo->description = '';
        }

        return $groupeInfo;
    }


    function api_explorer($routes,$url){
        require_once('API_Explorer/explorer.php');
    }


    function parseRouteAnnotation($annotation)
    {
        $routeInfo = new stdClass(); // Création d'un objet pour stocker les informations
        $routeInfo->parameters = []; // Ajout de la propriété $parameters par défaut

        // Vérification de la présence de l'annotation
        if (!preg_match('/@Route\s+/', $annotation)) {
            return null; // Retourne null si l'annotation n'est pas valide
        }

        // Extraction de la méthode
        if (preg_match('/- Method: (\w+)/', $annotation, $methodMatch)) {
            $routeInfo->method = strtoupper($methodMatch[1]); // Assure que la méthode est en majuscules
        } else {
            return null; // Retourne null si la méthode n'est pas trouvée
        }

        // Extraction du chemin
        if (preg_match('/- Path: ([^\s]+)/', $annotation, $routeMatch)) {
            $routeInfo->route = trim($routeMatch[1]); // Assure que le chemin est sans espaces indésirables
        } else {
            return null; // Retourne null si le chemin n'est pas trouvé
        }

        // Extraction de la description
        if (preg_match('/- Description: ([^\r\n]+)/', $annotation, $descriptionMatch)) {
            $routeInfo->description = trim($descriptionMatch[1]);
        }else {
            $routeInfo->description = '';
        }

        // Extraction des paramètres
        if (preg_match('/- Parameters: {([^}]+)}/', $annotation, $parameterMatch)) {
            $parameterInfo = [];
            $parameterLines = explode("\n", $parameterMatch[1]);

            foreach ($parameterLines as $line) {
                if (preg_match('/- (\w+): \((\w+)\) (?:ex:\((\w+)\))?(.+)?/', $line, $parameterMatch)) {
                    $paramName = $parameterMatch[1];
                    $parameterInfo[$paramName] = [
                        'name' => $paramName,  // Ajout du nom du paramètre
                        'type' => $parameterMatch[2],
                        'exemple' => isset($parameterMatch[3]) ? trim($parameterMatch[3]) : null, // exemple est optionnelle
                        'description' => isset($parameterMatch[4]) ? trim($parameterMatch[4]) : '', // Description est optionnelle
                    ];
                }
            }

            $routeInfo->parameters = $parameterInfo;
        }

        if (preg_match('/- Security/', $annotation)) {
            $routeInfo->security = true;
        } else {
            $routeInfo->security = false;
        }

        return $routeInfo;
    }
}