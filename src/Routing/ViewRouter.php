<?php

namespace EvoltyFramework\Routing;

use EvoltyFramework\Routing\Annotation;
use EvoltyFramework\Tools\Log;
use EvoltyFramework\Tools\Env;
use Exception;

class ViewRouter
{
    private $logGlobal;
    private $initURL = "/api";
    private $routes = [];
    private $api_routes = [];
    private $view_routes = [];
    private $groupe_annotations = [];

    public function __construct()
    {
        $this->logGlobal = new Log();
        $env = new Env();
    }

    public function addRoute($method, $path, $callback)
    {
        $this->routes[$method][$path] = $callback;

        $reflection = new \ReflectionFunction($callback);
        $comments = $reflection->getDocComment();

        $annotation = new Annotation;
        $routeInfo = $annotation->parseRouteAnnotation($comments);

        if ($routeInfo) {
            $this->api_routes[] = $routeInfo;
        }
    }

    public function addViewRoute($path, $view)
    {
        $this->view_routes[$path] = $view;
    }

    public function get($path, $callback)
    {
        $this->addRoute('GET', $path, $callback);
    }

    public function post($path, $callback)
    {
        $this->addRoute('POST', $path, $callback);
    }


    public function route()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $path = str_replace($this->initURL, '', $path);

        // Filtrer et valider l'URL
        $path = filter_var($path, FILTER_SANITIZE_URL);

        // Gérer la méthode HTTP
        switch ($method) {
            case 'GET':
                $params = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
                break;
            case 'POST':
                $input = file_get_contents('php://input');
                $params = json_decode($input, true);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    $params = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['error' => 'Method not found']);
                return;
        };

        // Vérifier si c'est une route
        if (isset($this->routes[$method][$path])) {
            $callback = $this->routes[$method][$path];

            if (is_callable($callback)) {
                try {
                    call_user_func($callback, $params);
                } catch (Exception $e) {
                    http_response_code(500);
                    $this->logGlobal->addLog($e);
                    echo json_encode(['error' => $e->getMessage()]);
                }
            } else {
                http_response_code(500);
                $this->logGlobal->addLog('Internal server error');
                echo json_encode(['error' => 'Internal server error']);
            }
        } elseif (isset($this->view_routes[$path])) {
            // Gérer les routes de vue
            $this->logGlobal->addLog('Page not found 2 ');
            $view = $this->view_routes[$path];
            $this->renderView($view, $params);
        } else {
            http_response_code(404);
            $this->logGlobal->addLog('Page not found');
            echo json_encode(['error' => 'Page not found']);
        }
    }

    public function renderView($view, $params)
    {
        if (file_exists($view)) {
            extract($params);
            include $view;
        } else {
            http_response_code(500);
            $this->logGlobal->addLog('View not found');
            echo json_encode(['error' => 'View not found']);
        }
    }

}
