<?php

namespace EvoltyFramework\Routing;

use EvoltyFramework\Routing\Annotation;
use EvoltyFramework\Tools\Log;
use EvoltyFramework\Tools\Env;
use Exception;

class ApiRouter
{
    private $logGlobal;
    // private $initURL="/RestAPI";
    private $initURL="/api";

    public function __construct()
    {
        $this->logGlobal = new Log();
        $env = new Env();
    }

    private $routes = [];
    private $api_routes = [];
    private $groupe_annotations = [];

    public function APIExplorerRoute() {
        $groupeAnnotations = $this->groupe_annotations;
        $apiRoutes = $this->api_routes;
        $sortedRoutes = [];
        $notDefinedGroup = 'Not Defined';
    
        // Parcourir les annotations générales pour identifier les groupes
        foreach ($groupeAnnotations as $annotation) {
            $group = $annotation->groupe;
            
            // Initialiser le groupe dans sortedRoutes s'il n'existe pas
            if (!isset($sortedRoutes[$group])) {
                $sortedRoutes[$group] = (object) [
                    'groupe' => $group,
                    'description' => $annotation->description,
                    'routes' => []
                ];
            }
    
            // Parcourir les routes de l'API pour ajouter les routes correspondantes au groupe
            foreach ($apiRoutes as $route) {
                $routeGroup = $route->groupe === 'Not Defined' ? $notDefinedGroup : $route->groupe;
                if ($routeGroup === $group) {
                    $sortedRoutes[$group]->routes[] = $route;
                }
            }
        }
    
        // Ajouter un groupe "Not Defined" s'il y a des routes non définies
        foreach ($apiRoutes as $route) {
            if ($route->groupe === 'Not Defined') {
                if (!isset($sortedRoutes[$notDefinedGroup])) {
                    $sortedRoutes[$notDefinedGroup] = (object) [
                        'groupe' => $notDefinedGroup,
                        'description' => 'Routes without a defined group',
                        'routes' => []
                    ];
                }
                $sortedRoutes[$notDefinedGroup]->routes[] = $route;
            }
        }
        
        // $this->logGlobal->addArrayLog(array_values($sortedRoutes));
        $annotation = new Annotation;
        $annotation->api_explorer($sortedRoutes, $this->initURL);
    }
    

    public function addRoute($method, $path, $callback)
    {
        $this->routes[$method][$path] = $callback;


        $reflection = new \ReflectionFunction($callback);
        $comments = $reflection->getDocComment();

        $annotation = new Annotation;
        $routeInfo = $annotation->parseAPIRouteAnnotation($comments);

        if ($routeInfo) {
            $this->api_routes[] = $routeInfo;
        }

    }

    public function addGroupe($callback)
    {
        $reflection = new \ReflectionFunction($callback);
        $comments = $reflection->getDocComment();

        $annotation = new Annotation;
        $groupeInfo = $annotation->parseGeneralAnnotation($comments);

        if ($groupeInfo) {
            $this->groupe_annotations[] = $groupeInfo;
        }
    }


    public function get($path, $callback)
    {
        $this->addRoute('GET', $path, $callback);
    }

    public function post($path, $callback)
    {
        $this->addRoute('POST', $path, $callback);
    }

    public function delete($path, $callback)
    {
        $this->addRoute('DELETE', $path, $callback);
    }

    public function put($path, $callback)
    {
        $this->addRoute('PUT', $path, $callback);
    }

    public function route()
    {
        header('Content-Type: application/json');
        $method = $_SERVER['REQUEST_METHOD'];
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $path = str_replace($this->initURL, '', $path);
        // Gérer la méthode HTTP
        switch ($method) {
            case 'GET':
                $params = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
                break;
            case 'POST':
                $input = file_get_contents('php://input');
                $params = json_decode($input, true);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    $params = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
                }
                break;
            case 'PUT':
                $params = json_decode(file_get_contents("php://input"), true);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    http_response_code(400);
                    echo json_encode(['error' => 'Invalid JSON in PUT request']);
                    return;
                }
                break;
            case 'DELETE':
                $params = json_decode(file_get_contents("php://input"), true);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    http_response_code(400);
                    echo json_encode(['error' => 'Invalid JSON in DELETE request']);
                    return;
                }
                break;
            default:
                http_response_code(404);
                echo json_encode(['error' => 'Method not found']);
                return;
        }
        // $this->logGlobal->addArrayLog(array_values($this->routes));
        if (isset($this->routes[$method][$path])) {
            $callback = $this->routes[$method][$path];

            if (is_callable($callback)) {
                try {
                    call_user_func($callback, $params);
                    // echo json_encode(['error' => is_callable($callback)]);
                } catch (Exception $e) {
                    http_response_code(500);
                    $this->logGlobal->addLog($e);
                    echo json_encode(['error' => $e]);
                }
            } else {
                http_response_code(500);
                $this->logGlobal->addLog('Internal server error');
                echo json_encode(['error' => 'Internal server error']);
            }
        } else {
            http_response_code(404);
            $this->logGlobal->addLog('Page not found');
            echo json_encode(['error' => 'Page not found']);
        }
    }


}