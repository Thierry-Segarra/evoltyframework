<?php

namespace EvoltyFramework;

class Config
{
    protected static $config = [];

    public static function load($configPath)
    {
        self::$config = include $configPath;
    }

    public static function get($key, $default = null)
    {
        return self::$config[$key] ?? $default;
    }
}
