<?php

// Chemins de recherche pour autoload.php
$autoloadPaths = [
    __DIR__ . '/../../../../../autoload.php',  // Premier chemin
    __DIR__ . '/../../../vendor/autoload.php'  // Second chemin
];

$autoloadFound = false;
foreach ($autoloadPaths as $path) {
    if (file_exists($path)) {
        require $path;
        $autoloadFound = true;
        break;
    }
}

if (!$autoloadFound) {
    die("Autoloader non trouvé. Vérifiez les chemins et assurez-vous que les dépendances sont installées.\n");
}

try {
    // Initialiser la classe Env pour charger les variables d'environnement
    $env = new EvoltyFramework\Tools\Env();

    // Récupérer l'environnement actuel (dev par défaut)
    $environment = $_ENV['ENVIRONMENT'] ?? 'dev';

    // Séparation dev et prod
    if ($environment === 'dev') {
        $port = $_ENV['PORT_API'] ?? 8000;

        // Check if REWRITE_BASE is set
        if (!$port) {
            throw new Exception("PORT_WEB environment variable is not set.");
        }
    
        // Vérifier si PHP est en version 7.4 ou supérieure
        if (version_compare(PHP_VERSION, '7.4.0', '<')) {
            die("PHP 7.4 ou supérieur est requis pour exécuter ce serveur.\n");
        }
    
        // Define the public directory
        $publicDirPaths = [
            __DIR__ . '/../../../../../../public_api',  // First path
            __DIR__ . '/../../../public_api'         // Second path
        ];
    
        $publicDirFound = false;
        foreach ($publicDirPaths as $path) {
            if (is_dir($path)) {
                $publicDir = $path;
                $publicDirFound = true;
                break;
            }
        }
    
        if (!$publicDirFound) {
            die("The public directory does not exist in any of the specified paths.\n");
        }
    
        // Désactiver le timeout
        set_time_limit(0); // 0 signifie aucune limite
        ini_set('memory_limit', '-1'); // -1 signifie aucune limite de mémoire
    
        // Démarrer le serveur intégré PHP
        $command = sprintf(
            'php -S %s:%d -t %s',
            'localhost',
            $port,
            $publicDir
        );
    
        echo "Démarrage du serveur de développement sur http://localhost:$port\n";
        echo "Appuyez sur Ctrl-C pour arrêter le serveur.\n";
    
        // Exécuter la commande pour démarrer le serveur
        passthru($command);

    } elseif ($environment === 'prod') {
        // Environnement de production
        // ini_set('display_errors', '0');
        // error_reporting(E_ALL);

        echo "Environnement de production détecté.\n";
        echo "Veuillez utiliser Apache ou Nginx pour servir l'application.\n";
    } else {
        die("Environnement non reconnu : $environment. Utilisez 'dev' ou 'prod'.\n");
    }

} catch (\Throwable $th) {
    if ($environment === 'dev') {
        echo "Erreur : " . $th->getMessage() . "\n";
    } else {
        error_log("Erreur : " . $th->getMessage());  // Enregistrer les erreurs dans les logs en prod
        echo "Une erreur est survenue. Veuillez vérifier les logs du serveur.\n";
    }
}
