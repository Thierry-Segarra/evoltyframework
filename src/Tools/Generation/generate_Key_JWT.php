<?php
$autoloadPaths = [
    __DIR__ . '/../../../../../autoload.php',  // First path
    __DIR__ . '/../../../vendor/autoload.php'  // Second path
];

$autoloadFound = false;
foreach ($autoloadPaths as $path) {
    if (file_exists($path)) {
        require $path;
        $autoloadFound = true;
        break;
    }
}

try {
    $key = generateRandomKey();

    echo "KEY JWT :\n";
    echo $key;

} catch (\Throwable $th) {
    echo "Error: " . $th->getMessage() . "\n";
}

/**
 * Function to generate a random key
 * @param int $length Length of the key in bytes
 * @return string Base64 encoded random key
 */
function generateRandomKey($length = 128) {
    $randomBytes = openssl_random_pseudo_bytes($length);
    return base64_encode($randomBytes);
}
