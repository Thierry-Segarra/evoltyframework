<?php

namespace EvoltyFramework\Tools;

use PDO;
use PDOException;
use EvoltyFramework\Database\DatabaseManager;
use EvoltyFramework\Tools\Log;

class Model {
    protected $tableName;
    protected $columns;
    protected $dbManager;

    protected $log;

    public function __construct($tableName, $columns) {
        $this->tableName = $tableName;
        $this->columns = $columns;
        $this->dbManager = DatabaseManager::getInstance();
        $this->log = new Log();
    }

    public function getDbManager() {
        return $this->dbManager;
    }

    public function findOne($options = []) {
        $whereClause = $this->buildWhereClause($options['where'] ?? []);
        $attributes = $this->buildAttributes($options['attributes'] ?? []);
        $orderClause = $this->buildOrderClause($options['order'] ?? []);

        $query = "SELECT $attributes FROM $this->tableName $whereClause $orderClause LIMIT 1";

        $stmt = $this->dbManager->getDb()->prepare($query);
        try {
            $stmt->execute($options['where'] ?? []);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            $this->log->addLog($e);
            return [];
        }
    }

    public function findAll($options = []) {
        $whereClause = $this->buildWhereClause($options['where'] ?? []);
        $attributes = $this->buildAttributes($options['attributes'] ?? []);
        $orderClause = $this->buildOrderClause($options['order'] ?? []);
        $limitClause = $this->buildLimitClause($options['limit'] ?? null);

        $query = "SELECT $attributes FROM $this->tableName $whereClause $orderClause $limitClause";

        $stmt = $this->dbManager->getDb()->prepare($query);
        try {
            $stmt->execute($options['where'] ?? []);
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            $this->log->addLog($e);
            return [];
        }
    }

    public function findByPk($id, $attributes = []) {
        // Détecter dynamiquement la clé primaire de la table
        $primaryKey = $this->getPrimaryKey();
    
        if (!$primaryKey) {
            $this->log->addLog("Erreur : Impossible de détecter la clé primaire pour la table $this->tableName.");
            return ['erreur' => "Clé primaire non définie pour la table $this->tableName."];
        }
    
        // Construire les colonnes à récupérer
        $columns = empty($attributes) ? "*" : implode(", ", $attributes);
    
        // Construire la requête SQL pour rechercher par clé primaire
        $query = "SELECT $columns FROM $this->tableName WHERE $primaryKey = :primaryKey LIMIT 1";
    
        $stmt = $this->dbManager->getDb()->prepare($query);
    
        try {
            // Exécuter la requête avec le paramètre `id`
            $stmt->execute(['primaryKey' => $id]);
    
            // Récupérer le résultat sous forme de tableau associatif
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            // Enregistrer l'erreur dans les logs
            $this->log->addLog("Erreur dans findByPk: " . $e->getMessage());
            return ['erreur' => 'Erreur lors de la récupération de l\'enregistrement: ' . $e->getMessage()];
        }
    }
    

    private function buildWhereClause($conditions) {
        $whereClause = "";
        if (!empty($conditions)) {
            $whereClause = "WHERE ";
            foreach ($conditions as $key => $value) {
                $whereClause .= "$key = :$key AND ";
            }
            $whereClause = rtrim($whereClause, " AND ");
        }
        return $whereClause;
    }

    private function buildAttributes($attributes) {
        if (empty($attributes)) {
            return "*"; // Select all columns by default
        }
        return implode(", ", $attributes);
    }

    private function buildOrderClause($order) {
        if (empty($order)) {
            return ""; // No ordering
        }
        $orderBy = [];
        foreach ($order as $column => $direction) {
            $direction = strtoupper($direction) === 'DESC' ? 'DESC' : 'ASC';
            $orderBy[] = "$column $direction";
        }
        return "ORDER BY " . implode(", ", $orderBy);
    }

    private function buildLimitClause($limit) {
        if ($limit === null) {
            return ""; // No limit
        }
        return "LIMIT $limit";
    }

    public function create(array $data) {
        if (empty($data)) {
            $this->log->addLog("Erreur: Les données fournies pour l'insertion sont vides.");
            return ['erreur' => 'Les données fournies sont vides.'];
        }
    
        // Construire les colonnes et les valeurs
        $columns = implode(", ", array_keys($data));
        $values = ":" . implode(", :", array_keys($data));
    
        $query = "INSERT INTO $this->tableName ($columns) VALUES ($values)";
        $stmt = $this->dbManager->getDb()->prepare($query);
    
        try {
            $stmt->execute($data);
    
            // Retourne l'ID nouvellement inséré
            $lastInsertId = $this->dbManager->getDb()->lastInsertId();
            return ['result' => 'Insertion réussie', 'id' => $lastInsertId];
        } catch (PDOException $e) {
            // Journalisation de l'erreur
            $this->log->addLog("Erreur lors de l'insertion: " . $e->getMessage());
            return ['erreur' => 'Erreur lors de l\'insertion: ' . $e->getMessage()];
        }
    }
    

    public function update($id, $data) {
        // Récupérer la clé primaire de la table
        $primaryKey = $this->getPrimaryKey();
    
        if (!$primaryKey) {
            $this->log->addLog("Erreur: La clé primaire n'a pas pu être détectée.");
            return false;
        }
    
        // Construire la clause SET pour la mise à jour
        $setClause = "";
        foreach ($data as $key => $value) {
            $setClause .= "$key = :$key, ";
        }
        $setClause = rtrim($setClause, ", "); // Supprimer la dernière virgule
    
        // Créer la requête de mise à jour en utilisant la clé primaire dynamique
        $query = "UPDATE $this->tableName SET $setClause WHERE $primaryKey = :primaryKey";
    
        // Préparer et exécuter la requête
        $stmt = $this->dbManager->getDb()->prepare($query);
    
        try {
            // Ajouter l'ID à la donnée pour la condition WHERE
            $data['primaryKey'] = $id;
            $stmt->execute($data);
            return true;
        } catch (PDOException $e) {
            // Journaliser l'erreur
            $this->log->addLog("Erreur dans update: " . $e->getMessage());
            return false;
        }
    }
    

    public function delete($id) {
        $query = "DELETE FROM $this->tableName WHERE id = :id";

        $stmt = $this->dbManager->getDb()->prepare($query);
        try {
            $stmt->bindParam(":id", $id);
            $stmt->execute();
            return true;
        } catch (PDOException $e) {
            $this->log->addLog($e);
            return false;
        }
    }


    private function getPrimaryKey() {
        $query = "
            SELECT COLUMN_NAME 
            FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_NAME = :tableName 
            AND COLUMN_KEY = 'PRI'
            AND TABLE_SCHEMA = DATABASE()
        ";

        $stmt = $this->dbManager->getDb()->prepare($query);

        try {
            $stmt->execute(['tableName' => $this->tableName]);

            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            return $result['COLUMN_NAME'] ?? null; // Retourner le nom de la clé primaire
        } catch (PDOException $e) {
            $this->log->addLog("Erreur dans getPrimaryKey: " . $e->getMessage());
            return null;
        }
    }

}