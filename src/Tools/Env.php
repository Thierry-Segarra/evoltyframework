<?php

namespace EvoltyFramework\Tools;

use Dotenv\Dotenv;
use Exception;

class Env
{
    protected $dotenv = null;

    public function __construct() {
        $envPaths = [
            __DIR__ . '/../../../../../', // First path
            __DIR__ . '/../../'           // Second path
        ];
    
        $envFound = false;
        foreach ($envPaths as $path) {
            if (file_exists($path . '.env')) {
                $this->dotenv = Dotenv::createImmutable($path);
                $this->dotenv->load();
                $envFound = true;
                break;
            }
        }
    
        if (!$envFound) {
            throw new Exception("File .env Not Initialized\n");
        }
    }
    
}
