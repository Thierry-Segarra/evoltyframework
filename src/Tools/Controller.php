<?php

namespace EvoltyFramework\Tools;

use EvoltyFramework\Tools\Log;

class Controller
{
    // protected $model;

    // protected $log;

    // public function __construct($model = null)
    // {
    //     $this->model = $model;
    //     $this->log = new Log();
    // }

    // public function findAll($conditions = [])
    // {
    //     if ($this->model) {
    //         return $this->model->findAll($conditions);
    //     }
    //     return [];
    // }

    // public function findOne($conditions)
    // {
    //     if ($this->model) {
    //         return $this->model->findOne($conditions);
    //     }
    //     return [];
    // }

    // public function create($data)
    // {
    //     $this->log->addLog('model');
    //     $this->log->addLog($this->model);
    //     if ($this->model) {
    //         return $this->model->create($data);
    //     }
    //     return false;
    // }

    // public function update($id, $data)
    // {
    //     if ($this->model) {
    //         return $this->model->update($id, $data);
    //     }
    //     return false;
    // }

    // public function delete($id)
    // {
    //     if ($this->model) {
    //         return $this->model->delete($id);
    //     }
    //     return false;
    // }

    public function sanitizeInput($input)
    {
        $trimmedInput = trim($input);
        $sanitizedInput = htmlspecialchars($trimmedInput, ENT_QUOTES, 'UTF-8');
        return $sanitizedInput;
    }

    public function validateEmail($email)
    {
        $sanitizedEmail = $this->sanitizeInput($email);
        return filter_var($sanitizedEmail, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function checkNotEmpty($value)
    {
        $sanitizedValue = $this->sanitizeInput($value);
        return !empty($sanitizedValue);
    }

    public function hashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function verifyPassword($providedPassword, $hashedPassword)
    {
        return password_verify($providedPassword, $hashedPassword);
    }
}
