<?php

namespace EvoltyFramework\Tools;

class Log
{
    private $mainLogDirectory;
    private $subLogDirectory;
    private $logFilename;

    public function __construct($mainLogDirectory = __DIR__ . '/../../Log', $logFilename = 'trace.log')
    {
        $this->mainLogDirectory = rtrim($mainLogDirectory, '/') . '/';
        $this->logFilename = $logFilename;

        // Créer le sous-dossier avec la date du jour
        $this->subLogDirectory = $this->mainLogDirectory . date('Y-m-d') . '/';
    }

    public function setMainLogDirectory($mainLogDirectory)
    {
        // Vérifier si le chemin du dossier est valide
        if (is_dir($mainLogDirectory)) {
            $this->mainLogDirectory = rtrim($mainLogDirectory, '/') . '/';
            return true;
        } else {
            return false;
        }
    }

    public function setLogFilename($logFilename)
    {
        // Vérifier si le nom de fichier est valide
        if (!empty($logFilename)) {
            $this->logFilename = $logFilename;
            return true;
        } else {
            return false;
        }
    }

    private function ensureLogDirectoryExists()
    {
        // Créer le dossier principal s'il n'existe pas
        if (!file_exists($this->mainLogDirectory)) {
            mkdir($this->mainLogDirectory, 0777, true);
        }

        // Créer le sous-dossier avec la date du jour s'il n'existe pas
        if (!file_exists($this->subLogDirectory)) {
            mkdir($this->subLogDirectory, 0777, true);
        }
    }

    public function addLog($content)
    {
        $this->ensureLogDirectoryExists();

        $logFilePath = $this->subLogDirectory . DIRECTORY_SEPARATOR . $this->logFilename;

        // Ajouter un timestamp pour chaque entrée de log
        $timestamp = date('Y-m-d H:i:s');
        $logEntry = "[$timestamp] $content" . PHP_EOL;

        // Tentative d'ouverture du fichier en mode ajout
        $file = fopen($logFilePath, "a");
        if ($file) {
            // Écriture du contenu dans le fichier
            fwrite($file, $logEntry);
            // Fermeture du fichier
            fclose($file);
        }
    }

    public function addArrayLog($content)
    {
        $this->ensureLogDirectoryExists();

        $logFilePath = $this->subLogDirectory . DIRECTORY_SEPARATOR . $this->logFilename;

        // Convertir le contenu en chaîne de caractères si c'est un array
        if (is_array($content)) {
            $content = print_r($content, true);
        }

        // Ajouter un timestamp pour chaque entrée de log
        $timestamp = date('Y-m-d H:i:s');
        $logEntry = "[$timestamp] $content" . PHP_EOL;

        // Tentative d'ouverture du fichier en mode ajout
        $file = fopen($logFilePath, "a");
        if ($file) {
            // Écriture du contenu dans le fichier
            fwrite($file, $logEntry);
            // Fermeture du fichier
            fclose($file);
        }
    }
}
