<?php

namespace EvoltyFramework\Tools;

use EvoltyFramework\Config;
use EvoltyFramework\Tools\Env;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Exception;

class Session
{
    private $secret_key;
    private $alg;
    private $token_expiration;
    private $refresh_token_expiration;
    
    // Factory method to get an instance of Session
    public static function getInstance()
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new Session();
        }
        return $instance;
    }

    // Make constructor private to prevent direct instantiation
    private function __construct()
    {
        new Env();

        $this->secret_key = $_ENV['JWT_KEY'];
        $this->alg = $_ENV['JWT_ALGORITHME'];
        $this->token_expiration = $_ENV['JWT_TOKEN_EXPIRATION'];
        $this->refresh_token_expiration = $_ENV['JWT_REFRESH_TOKEN_EXPIRATION'];
        
    }
    
    
    // Fonction pour générer un jeton JWT
    public function generateJWT($data)
    {
        $payload = $data;
        $payload['exp'] = time() + $this->token_expiration;
        return JWT::encode($payload, $this->secret_key, $this->alg);
    }
    
    // Fonction pour vérifier et décoder le jeton JWT
    public function verifyJWT($jwt)
    {
        try {
            $decoded = JWT::decode($jwt, new Key($this->secret_key, $this->alg));
            return $decoded; // Retourne l'objet décodé
        } catch (\Firebase\JWT\ExpiredException $e) {
            return 'expired'; // Jeton expiré
        } catch (\Firebase\JWT\SignatureInvalidException $e) {
            return 'invalid_signature'; // Signature invalide
        } catch (Exception $e) {
            return null; // Autres erreurs
        }
    }


     // Fonction pour générer un jeton de rafraîchissement
    public function generateRefreshToken($data)
    {
        $payload = $data;
        $payload['exp'] = time() + $this->refresh_token_expiration;
        return JWT::encode($payload, $this->secret_key, $this->alg);
    }
    
    // Méthode pour vérifier et rafraîchir le token si nécessaire
    public function verifyAndRefreshToken($jwt, $refreshToken)
    {
        $decoded_token = $this->verifyJWT($jwt);

        if ($decoded_token && $decoded_token !== 'expired') {
            return $decoded_token; // Token principal valide et non expiré
        } elseif ($decoded_token === 'expired') {
            $decoded_refresh_token = $this->verifyJWT($refreshToken);
            if ($decoded_refresh_token && $decoded_refresh_token !== 'expired') {
                // Générez un nouveau token principal et renvoyez-le
                return $this->generateJWT((array) $decoded_refresh_token);
            } else {
                return 'refresh_token_invalid'; // Jeton de rafraîchissement invalide ou expiré
            }
        } else {
            return 'token_invalid'; // Token principal invalide
        }
    }


    public function getSession()
    {
        // Récupérer le token JWT de l'en-tête Authorization
        $headers = getallheaders();
        $authHeader = $headers['Authorization'] ?? null;

        if ($authHeader) {
            // Extraire le token de l'en-tête (en supposant que le format soit "Bearer {token}")
            list($type, $token) = explode(' ', $authHeader, 2);

            if (strcasecmp($type, 'Bearer') == 0 && !empty($token)) {
                $data = $this->verifyJWT($token);

                if ($data) {
                    return [
                        'authenticated' => true,
                        'data' => $data
                    ];
                } else {
                    return [
                        'authenticated' => false,
                        'error' => 'Invalid or expired token'
                    ];
                }
            } else {
                return ['error' => 'Invalid Authorization header format'];
            }
        } else {
            return ['error' => 'Missing Authorization header'];
        }
    }
}
